const sumarNumeros = (numUno, numDos) =>{
    return numUno + numDos
}

const verificarSuma = (numUno, numDos, valorEsperado) =>{
    const sumNum = sumarNumeros(numUno, numDos)
    if(sumNum === valorEsperado){
        console.log(`La suma de los números ${numUno} y ${numDos} es igual a ${valorEsperado}`)
    }else{
        throw new Error (`La suma de los números ${numUno} y ${numDos} es diferente a ${valorEsperado}`)
    }
}

verificarSuma(4, 2, 6)